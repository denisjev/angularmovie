import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { ListMoviesComponent } from './components/list-movies/list-movies.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { TrailerComponent } from './components/trailer/trailer.component';

const routes: Routes = [      
  { path: '', component: HomeComponent },
  { path: 'movies-list', component: ListMoviesComponent },
  { path: 'movie-trailer/:id', component: TrailerComponent },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
