import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { Movie } from './../../models/movie';

@Component({
  selector: 'app-movie',
  templateUrl: './movie.component.html',
  styleUrls: ['./movie.component.css']
})
export class MovieComponent implements OnInit {

  @Input()
  currentMovie?: Movie;
  
  @Output()
  deleteMovieEventFromParent = new EventEmitter<Movie>();

  isFavorite: boolean = false;
  seeMoreSelected: boolean = false;
  textButtonSeeMore: string = "Ver más";

  constructor() { }

  ngOnInit(): void {
    console.log(this.currentMovie);
  }

  seeMore() {
    this.seeMoreSelected = !this.seeMoreSelected;
    if(this.seeMoreSelected) this.textButtonSeeMore = "Ocultar"
    else this.textButtonSeeMore = "Ver más";
  }

  addFavorite() {
    this.isFavorite = true;
  }

  deleteMovieComponent() {
    this.deleteMovieEventFromParent.emit(this.currentMovie);
  }
}
