import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Movie } from '../../models/movie';
import { Validators } from '@angular/forms';

@Component({
  selector: 'app-movie-reactive-form',
  templateUrl: './movie-reactive-form.component.html',
  styleUrls: ['./movie-reactive-form.component.css']
})
export class MovieReactiveFormComponent implements OnInit {

  movieForm = this.fb.group({
    title: ['', Validators.required],
    year: ['', Validators.required],
    runtime: ['', Validators.required],
    genre: ['', Validators.required],
    plot: ['', Validators.required],
    released: ['', Validators.required],
    poster: ['', Validators.required]
  });

  genres = ['Accion','Romance','Aventura','Familiar'];

  @Output()
  addMovieEventFromParent = new EventEmitter<Movie>();

  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
  }

  onSubmit() {
    this.addMovieEventFromParent.emit(this.movieForm.value);
  }

}
