import { Component, OnInit } from '@angular/core';
import { Movie } from '../../models/movie';
import { MovieService } from '../../services/movie.service';

@Component({
  selector: 'app-list-movies',
  templateUrl: './list-movies.component.html',
  styleUrls: ['./list-movies.component.css']
})
export class ListMoviesComponent implements OnInit {
  movieToFind: string = "";
  movieListObject?:Movie[];
  showNewMovie = false;

  constructor(private movieService: MovieService) { }

  ngOnInit(): void {
    this.movieService.getMovies().subscribe(data => this.movieListObject = data);
  }

  findMovie() {
    this.movieService.findMovies(this.movieToFind).subscribe(data => this.movieListObject = data);
  }

  resetListMovie() {
    this.movieService.getMovies().subscribe(data => this.movieListObject = data);
    this.movieToFind = "";
  }

  deleteMovie(movie: Movie) {
    this.movieService.deleteMovie(movie.id).subscribe(data => {
      console.log(data);
      this.movieService.getMovies().subscribe(data => this.movieListObject = data);
    })
  }

  showAddMovieForm() {
    this.showNewMovie = !this.showNewMovie;
  }
  
  addMovie(movie: Movie) {
    this.movieService.addMovie(movie).subscribe(data => {
      movie.id = data;
      this.movieListObject?.push(movie);
    });
    this.showAddMovieForm();
  }

}
