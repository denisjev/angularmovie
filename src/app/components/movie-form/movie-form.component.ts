import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Movie } from '../../models/movie';

@Component({
  selector: 'app-movie-form',
  templateUrl: './movie-form.component.html',
  styleUrls: ['./movie-form.component.css']
})
export class MovieFormComponent implements OnInit {

  model = new Movie();
  genres = ['Accion','Romance','Aventura','Familiar'];

  @Output()
  addMovieEventFromParent = new EventEmitter<Movie>();
  constructor() { }

  ngOnInit(): void {
  }

  newMovie() {
    this.model = new Movie();
  }

  onSubmit() {
    this.addMovieEventFromParent.emit(this.model);
    this.model = new Movie();
  }
}
