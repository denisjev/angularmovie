import { InfoSite } from '../models/infoSite';

export const infoSiteJson: InfoSite = {
    title: "Mi sitio de peliculas",
    description: "Aplicación para ver las mejores películas", 
    year: 2021
}