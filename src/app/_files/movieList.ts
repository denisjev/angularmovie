import { Movie } from './../models/movie';

export const MovieListJson: Movie[] =
[
    {
      "id": 1,
      "title": "Avatar",
      "year": "2009",
      "released": "18 Dec 2009",
      "runtime": "162 min",
      "genre": "Action, Adventure, Fantasy",
      "plot": "A paraplegic marine dispatched to the moon Pandora on a unique mission becomes torn between following his orders and protecting the world he feels is his home.",
      "poster": "https://images-na.ssl-images-amazon.com/images/M/MV5BMjEyOTYyMzUxNl5BMl5BanBnXkFtZTcwNTg0MTUzNA@@._V1_SX1500_CR0,0,1500,999_AL_.jpg",
    },
    {
      "id": 2,
      "title": "300",
      "year": "2006",
      "released": "09 Mar 2007",
      "runtime": "117 min",
      "genre": "Action, Drama, Fantasy",
      "plot": "King Leonidas of Sparta and a force of 300 men fight the Persians at Thermopylae in 480 B.C.",
      "poster": "https://images-na.ssl-images-amazon.com/images/M/MV5BMTMwNTg5MzMwMV5BMl5BanBnXkFtZTcwMzA2NTIyMw@@._V1_SX1777_CR0,0,1777,937_AL_.jpg"
    },
    {
      "id": 3,
      "title": "The Avengers",
      "year": "2012",
      "released": "04 May 2012",
      "runtime": "143 min",
      "genre": "Action, Sci-Fi, Thriller",
      "plot": "Earth's mightiest heroes must come together and learn to fight as a team if they are to stop the mischievous Loki and his alien army from enslaving humanity.",
      "poster": "https://images-na.ssl-images-amazon.com/images/M/MV5BMTA0NjY0NzE4OTReQTJeQWpwZ15BbWU3MDczODg2Nzc@._V1_SX1777_CR0,0,1777,999_AL_.jpg"
    }
  ]