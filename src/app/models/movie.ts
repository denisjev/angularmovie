export class Movie {
    
    constructor(
        public id: number = 0,
        public title: string = "",
        public year: string = "",
        public runtime: string = "",
        public genre: string = "",
        public plot: string = "",
        public released: string = "",
        public poster: string = ""
    )
    {}
}