import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MovieComponent } from './components/movie/movie.component';
import { MenuComponent } from './components/menu/menu.component';
import { MovieFormComponent } from './components/movie-form/movie-form.component';
import { MovieReactiveFormComponent } from './components/movie-reactive-form/movie-reactive-form.component';
import { HomeComponent } from './components/home/home.component';
import { ListMoviesComponent } from './components/list-movies/list-movies.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { TrailerComponent } from './components/trailer/trailer.component';


@NgModule({
  declarations: [
    AppComponent,
    MovieComponent,
    MenuComponent,
    MovieFormComponent,
    MovieReactiveFormComponent,
    HomeComponent,
    ListMoviesComponent,
    PageNotFoundComponent,
    TrailerComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
