import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { Movie } from '../models/movie';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    Authorization: 'my-auth-token'
  })
};

@Injectable({
  providedIn: 'root'
})
export class MovieService {
  moviesUrl = 'https://localhost:5001/apimovie';

  constructor(private http: HttpClient) { }

  getMovies() {
    return this.http.get<Movie[]>(this.moviesUrl)
              .pipe(retry(3), catchError(this.handleError));
  }

  getMovie(id: string) {
    const url = `${this.moviesUrl}/${id}`;
    return this.http.get<Movie>(url)
              .pipe(retry(3), catchError(this.handleError));
  }

  findMovies(title: string) {
    const url = `${this.moviesUrl}/findtitle/${title}`;
    return this.http.get<Movie[]>(url)
              .pipe(retry(3), catchError(this.handleError));
  }

  addMovie(movie: Movie): Observable<number> {
    return this.http.post<number>(this.moviesUrl, movie, httpOptions)
      .pipe(catchError(this.handleError));
  }

  deleteMovie(id: number): Observable<unknown> {
    const url = `${this.moviesUrl}/${id}`;
    return this.http.delete(url)
      .pipe(catchError(this.handleError));
  }

  private handleError(error: HttpErrorResponse) {
    if (error.status === 0) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong.
      console.error(
        `Backend returned code ${error.status}, body was: `, error.error);
    }
    // Return an observable with a user-facing error message.
    return throwError(
      'Something bad happened; please try again later.');
  }
}
